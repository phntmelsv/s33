// 3. retrieve all to do list items
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => console.log(json))

// 4. map method: title of every item


fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
          let all = data.map(todo => {
                return todo.title
              })

          console.log(all)
    })

// 5. single to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => console.log(json))


// 6. console message
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => console.log(json.title))

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => console.log(json.status))

// 7. fetch request: POST
fetch('https://jsonplaceholder.typicode.com/todos', {
method: 'POST',
headers: {
'Content-Type': 'application/json'
},
body: JSON.stringify({
completed: true,
title: 'fix dashboard backend',
userId: 1
})
})
.then(response => response.json())
.then(json => console.log(json))

// 8. fetch request: PUT
fetch('https://jsonplaceholder.typicode.com/todos/1', {
method: 'PUT',
headers: {
'Content-Type': 'application/json'
},
body: JSON.stringify({
completed: false,
title: 'query and code review'
})
})
.then(response => response.json())
.then(json => console.log(json))

// 9. change the data scructure
fetch('https://jsonplaceholder.typicode.com/todos/5', {
method: 'PUT',
headers: {
'Content-Type': 'application/json'
},
body: JSON.stringify({
title: 'alignment meeting with stakeholders',
description: 'meeting is to decide on dashboard metrics',
status: 'in progress',
dateCompleted: '2023-04-22',
userId: 3
})
})
.then(response => response.json())
.then(json => console.log(json))

// 10. fetch request: PATCH
fetch('https://jsonplaceholder.typicode.com/todos/4', {
method: 'PATCH',
headers: {
'Content-Type': 'application/json'
},
body: JSON.stringify({
title: 'create forecasting model'
})
})
.then(response => response.json())
.then(json => console.log(json))

// 11. change status and add date
fetch('https://jsonplaceholder.typicode.com/todos/5', {
method: 'PATCH',
headers: {
'Content-Type': 'application/json'
},
body: JSON.stringify({
status: 'complete',
dateLastEdited: '2021-01-17'
})
})
.then(response => response.json())
.then(json => console.log(json))

// 12. fetch request: DELETE
fetch('https://jsonplaceholder.typicode.com/todos/7', {
method: 'DELETE',
})
.then(response => response.json())
.then(json => console.log(json))

// 13. Postman: retrieve all to do items

// 14. Postman: retrieve a single to do list item

// 15. Postman: create a to do list item